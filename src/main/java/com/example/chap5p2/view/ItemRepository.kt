package com.example.chap5p2.view

import com.example.chap5p2.database.ItemDao
import com.example.chap5p2.database.Items

class ItemRepository(private val itemDao: ItemDao) {
    suspend fun getItems(): List<Items> {
        return itemDao.getItems()
    }

    suspend fun insertNote(items: Items) {
        itemDao.insert(items)
    }

    suspend fun deleteItem(items: Items) {
        itemDao.delete(items)
    }
}