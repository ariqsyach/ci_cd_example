package com.example.chap5p2.presenter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.chap5p2.ui.start.FirstFragment
import com.example.chap5p2.ui.start.SecondFragment
import com.example.chap5p2.ui.start.ThirdFragment

class AdapterViewPager(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private val dataFragment = mutableListOf(
        FirstFragment(),
        SecondFragment(), ThirdFragment()
    )

    override fun getItem(position: Int): Fragment {
        return dataFragment[position]
    }

    override fun getCount(): Int {
        return dataFragment.size
    }
}
