package com.example.chap5p2.presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.chap5p2.R
import com.example.chap5p2.view.CallBack

class Controller(var callBack: CallBack) {

    var hasil :String = ""
    fun operasi(playerSatu: String, computer: String) {

        if (playerSatu == "gunting" && computer == "kertas" || playerSatu == "batu" && computer == "gunting" || playerSatu == "kertas" && computer == "batu") {
            hasil = "Menang"
            Log.d("hasil", "Player 1 Menang")
        } else if (playerSatu == "kertas" && computer == "gunting" || playerSatu == "gunting" && computer == "batu" || playerSatu == "batu" && computer == "kertas") {
            hasil = "Kalah"
            Log.d("hasil", "Player 2 Menang")
        } else if (playerSatu == "kertas" && computer == "kertas" || playerSatu == "gunting" && computer == "gunting" || playerSatu == "batu" && computer == "batu") {
            hasil = "Seri"
            Log.d("hasil", "Permainan Seri")
        }

    callBack.kirimBalik(hasil)
    }



}



