package com.example.chap5p2.ui.game

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.chap5p2.ui.navigation.BottomNavigationActivity
import com.example.chap5p2.presenter.Controller
import com.example.chap5p2.R
import com.example.chap5p2.database.HistoryDatabase
import com.example.chap5p2.database.Items
import com.example.chap5p2.database.HistoryAdapter
import com.example.chap5p2.ui.login.ExitActivity
import com.example.chap5p2.view.CallBack
import com.example.chap5p2.view.ItemRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.batu1
import kotlinx.android.synthetic.main.activity_main.batu2
import kotlinx.android.synthetic.main.activity_main.cpuName
import kotlinx.android.synthetic.main.activity_main.gunting1
import kotlinx.android.synthetic.main.activity_main.gunting2
import kotlinx.android.synthetic.main.activity_main.kertas1
import kotlinx.android.synthetic.main.activity_main.kertas2
import kotlinx.android.synthetic.main.activity_main.playerName
import kotlinx.android.synthetic.main.activity_main.tlstengah
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MultiplayerActivity : AppCompatActivity(),
    CallBack {
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var myList: MutableList<Items>
    private lateinit var database: HistoryDatabase
    private lateinit var repository: ItemRepository


    override fun kirimBalik(hasil: String) {
        val sp = getSharedPreferences("coba", Context.MODE_PRIVATE)
        val esp = sp.edit()
        saveBtn.visibility = View.VISIBLE
        esp.putString("hasil", "${hasil}")
        esp.commit()
        if (hasil == "Menang") {
            tlstengah.setImageResource(R.drawable.p1menang)

        } else if (hasil == "Kalah") {
            tlstengah.setImageResource(R.drawable.p2menang)
        } else {
            tlstengah.setImageResource(R.drawable.draw)
        }

    }


    @SuppressLint("ResourceAsColor")

    private val data = mutableListOf<String>()
    private var btnPlayer1 = mutableListOf<ImageView>()
    private var btnPlayer2 = mutableListOf<ImageView>()
    var control = Controller(this)
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        saveBtn.visibility = View.GONE

        val ubahNama = intent.getStringExtra("name")
        playerName.text = "${ubahNama}"
        cpuName.text = "Player 2"
        btnPlayer1 = mutableListOf(gunting1, batu1, kertas1)
        btnPlayer2 = mutableListOf(gunting2, batu2, kertas2)
        btnPlayer2.forEach {
            it.isEnabled = false
        }

        homeBtn.setOnClickListener {
            val intent = Intent(this@MultiplayerActivity, BottomNavigationActivity::class.java)
            val value = playerName.text.toString()
            intent.putExtra("name", value)
            startActivity(intent)
            Log.d("Start", "MultiplayerActivity")
        }
        exitButton.setOnClickListener {
            val intent = Intent(this@MultiplayerActivity, ExitActivity::class.java)
            startActivity(intent)
            Log.d("Exit", "Exit")
        }
        saveBtn.setOnClickListener {
            saveButton()
        }
    }

    private fun saveButton() {
        saveBtn.setImageResource(R.drawable.ic_save_active)
        database = HistoryDatabase.getDatabase(this)
        repository =
            ItemRepository(database.itemDao())
        myList = mutableListOf()
        historyAdapter = HistoryAdapter(myList)
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())
        val title = "${currentDate}"
        val sp = getSharedPreferences("coba", Context.MODE_PRIVATE)
        val esp = sp.edit()

        val nama = sp.getString("user_name", "")
        val hasil = sp.getString("hasil", "")
        esp.commit()
        val content = "${nama} ${hasil}"

        val item =
            Items(null, title, content)
        GlobalScope.launch {
            repository.insertNote(item)
        }
        loadItems()
        saveBtn.setOnClickListener {
            saveBtn.setImageResource(R.drawable.ic_save)
            Toast.makeText(this, "Tersimpan", Toast.LENGTH_LONG).show()
            deleteItems(item)
        }
    }
    private fun deleteItems(items: Items){
        GlobalScope.launch {
            repository.deleteItem(items)
        }
    }
    private fun loadItems() {
        //courotine
        GlobalScope.launch(Dispatchers.Main) {
            val items = repository.getItems()
            myList.clear()
            myList.addAll(items)
            historyAdapter.notifyDataSetChanged()
        }
    }


    fun resetbtn(v: View) {
        tlstengah.setImageResource(R.drawable.vs)
        btnPlayer1.forEach {
            it.isEnabled = true
            it.background = getDrawable(android.R.color.transparent)
        }
        btnPlayer2.forEach {
            it.background = getDrawable(android.R.color.transparent)
        }

        data.clear()
        Log.d("Reset", "MultiplayerActivity")
    }


    fun pilPlayerOne(v: View) {
        btnPlayer1.forEach {
            it.isEnabled = false
        }
        btnPlayer2.forEach {
            it.isEnabled = true
        }
        v.background = getDrawable(R.color.colorPrimaryDark)
        data.add(v.contentDescription.toString())
        Log.d("Player1", "Dipilih")
        Toast.makeText(this, "Silahkan Pilih Player 2", Toast.LENGTH_SHORT).show()
    }

    fun pilPlayerTwo(v: View) {
        btnPlayer2.forEach {
            it.isEnabled = false
        }
        control.operasi(data[0], v.contentDescription.toString())
        v.background = getDrawable(R.color.colorPrimaryDark)
//        saveBtn.visibility = View.VISIBLE
        Log.d("Player2", "Dipilih")
    }
}