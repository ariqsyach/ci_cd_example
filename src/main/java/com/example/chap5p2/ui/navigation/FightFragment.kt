package com.example.chap5p2.ui.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.chap5p2.R
//import com.example.chap5p2.ui.DataProfileActivity
import com.example.chap5p2.ui.game.MainActivity
import com.example.chap5p2.ui.game.MultiplayerActivity
import kotlinx.android.synthetic.main.activity_pilih.*

class FightFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container_nav: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fight, container_nav, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sp = activity!!.getSharedPreferences("coba", Context.MODE_PRIVATE)
     val esp = sp.edit()

        val name = sp.getString("user_name", "")
        esp.commit()
        txtVsPlyr.text = "${name} Bermain Melawan Player"
        txtVsComp.text = "${name} Bermain Melawan Computer"


        Glide.with(this)
            .load("https://i.ibb.co/3CwjgSN/vsplayer.png")
            .into(pilPlyr)

        Glide.with(this)
            .load("https://i.ibb.co/ZcRptjv/vscomputerr.png")
            .into(pilCom)

        Log.d("Start", "PilihActivity")


        pilCom.setOnClickListener {

            val intent = Intent(this@FightFragment.context, MainActivity::class.java)
            val ubahNama = name
            intent.putExtra("name", ubahNama)
            startActivity(intent)
            Log.d("Pilih", "VS Computer")
        }
        pilPlyr.setOnClickListener{
            val intent = Intent(this@FightFragment.context, MultiplayerActivity::class.java)
            val ubahNama = name
            intent.putExtra("name", ubahNama)
            startActivity(intent)
            Log.d("Pilih", "VS Player")
        }

    }

}
