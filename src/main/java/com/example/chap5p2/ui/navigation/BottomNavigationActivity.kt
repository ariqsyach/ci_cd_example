package com.example.chap5p2.ui.navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.chap5p2.R
import kotlinx.android.synthetic.main.activity_bottom_navigation.*

class BottomNavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        bottom_nav.itemIconTintList = null
        bottom_nav.setOnNavigationItemSelectedListener {
            when (it.itemId) {

                R.id.fight_menu -> {
                    navigateFragment(FightFragment())
                    supportActionBar?.title = "Fight"
                }
                R.id.history_menu -> {
                    navigateFragment(HistoryFragment())
                    supportActionBar?.title = "History"
                }
                R.id.profile_menu -> {
                    navigateFragment(ProfileFragment())
                    supportActionBar?.title = "Profile"
                }
            }
            true
        }
    }

    private fun navigateFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container_nav, fragment)
            commit()
        }
    }
}