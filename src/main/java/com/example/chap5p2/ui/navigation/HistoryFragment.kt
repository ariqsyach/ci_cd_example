package com.example.chap5p2.ui.navigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chap5p2.R
//import com.example.chap5p2.database.DatabaseAdapter
//import com.example.chap5p2.database.DatabaseContract
import com.example.chap5p2.database.HistoryDatabase
import com.example.chap5p2.database.Items
//import com.example.chap5p2.model.Item
import com.example.chap5p2.database.HistoryAdapter
import com.example.chap5p2.view.ItemRepository
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.item_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class HistoryFragment : Fragment() {
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var myList: MutableList<Items>
    private lateinit var database: HistoryDatabase
    private lateinit var repository: ItemRepository

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container_nav: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container_nav, false)
    }


    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        database = HistoryDatabase.getDatabase(this@HistoryFragment.context!!)
        repository = ItemRepository(database.itemDao())
        myList = mutableListOf()

        historyAdapter = HistoryAdapter(myList)
        rv_history.layoutManager =
            LinearLayoutManager(this@HistoryFragment.context!!, LinearLayoutManager.VERTICAL, false)
        rv_history.adapter = historyAdapter
        rv_history.setHasFixedSize(true)
        loadItems()
        tv_del_history.setOnClickListener{
          Toast.makeText(this@HistoryFragment.context,"Berhasil Menghapus",Toast.LENGTH_SHORT).show()
        }
    }


    private fun loadItems() {
        //courotine
        GlobalScope.launch(Dispatchers.Main) {
            val items = repository.getItems()
            myList.clear()
            myList.addAll(items)
            historyAdapter.notifyDataSetChanged()
        }
    }

    fun deleteItem(items: Items) {
        GlobalScope.launch(Dispatchers.Main) {
            repository.deleteItem(items)
            loadItems()


        }
    }


}






