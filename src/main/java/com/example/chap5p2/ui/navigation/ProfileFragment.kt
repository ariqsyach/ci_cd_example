package com.example.chap5p2.ui.navigation

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.chap5p2.R
import com.example.chap5p2.model.login.LoginResponse
import com.example.chap5p2.model.update.UpdateResponse
import com.example.chap5p2.model.update.UserUpdate
import com.example.chap5p2.network.ApiResource
import kotlinx.android.synthetic.main.dialog_edit_email.view.*
import kotlinx.android.synthetic.main.dialog_edit_username.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container_nav: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container_nav, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sp = activity!!.getSharedPreferences("coba", Context.MODE_PRIVATE)
        val esp = sp.edit()
        val idUser = sp.getString("user_id", "")
        esp.commit()
        getPersonById("${idUser}")

        et_emailProfile.setOnClickListener {
            editEmail()
        }
        et_unameProfile.setOnClickListener{
            editUname()
        }
    }


    private fun getPersonById(id: String) {
        ApiResource.create().getPersonById(id).enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                if (response.code() == 200) {
                    val tvemail = et_emailProfile as TextView
                    val tvuname = et_unameProfile as TextView
                    tvemail.text = "${response.body()?.data?.email}"
                    tvuname.text = "${response.body()?.data?.username}"
                }
            }
        })
    }

    private fun updatePerson(id: String, userUpdate: UserUpdate) {
        ApiResource.create().updatePerson(userUpdate, id)
            .enqueue(object : Callback<UpdateResponse> {
                override fun onFailure(call: Call<UpdateResponse>, t: Throwable) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(
                    call: Call<UpdateResponse>,
                    response: Response<UpdateResponse>
                ) {
                    if (response.code() == 200) {
                        val tvemail = et_emailProfile as TextView
                        val tvuname = et_unameProfile as TextView
                        tvemail.text = "${response.body()?.data?.email}"
                        tvuname.text = "${response.body()?.data?.username}"
                    }

                }
            })
    }


    private fun editUname() {
        this.let {
            val builder = AlertDialog.Builder(this@ProfileFragment.context)
            val inflater = this.layoutInflater
            val editUnameView = inflater.inflate(R.layout.dialog_edit_username, null)
            builder.setView(editUnameView)
            val dialog = builder.create()
            dialog.show()
            editUnameView.btn_updateuname.setOnClickListener {
                Toast.makeText(this@ProfileFragment.context, "Data di Update", Toast.LENGTH_SHORT).show()
                val uname = editUnameView.et_new_uname.text.toString()
                val updateUname = UserUpdate(username = "${uname}" )
                val sp = activity!!.getSharedPreferences("coba", Context.MODE_PRIVATE)
                val esp = sp.edit()
                val idUser = sp.getString("user_id", "")
                esp.commit()
                updatePerson("${idUser}", updateUname)
                dialog.dismiss()
            }
            editUnameView.btn_canceluname.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    private fun editEmail() {
        this.let {
            val builder = AlertDialog.Builder(this@ProfileFragment.context)
            val inflater = this.layoutInflater
            val editEmailView = inflater.inflate(R.layout.dialog_edit_email, null)
            builder.setView(editEmailView)
            val dialog = builder.create()
            dialog.show()
            editEmailView.btn_updateemail.setOnClickListener {
                Toast.makeText(this@ProfileFragment.context, "Data di Update", Toast.LENGTH_SHORT).show()
                val email = editEmailView.et_new_email.text.toString()
                val updateEmail = UserUpdate(email= "${email}")
                val sp = activity!!.getSharedPreferences("coba", Context.MODE_PRIVATE)
                val esp = sp.edit()
                val idUser = sp.getString("user_id", "")
                esp.commit()
                updatePerson("${idUser}", updateEmail)
                dialog.dismiss()
            }
            editEmailView.btn_cancelemail.setOnClickListener {
                dialog.dismiss()
            }


        }
    }
}
