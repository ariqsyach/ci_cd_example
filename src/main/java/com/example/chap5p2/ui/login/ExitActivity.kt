package com.example.chap5p2.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.chap5p2.R
import com.example.chap5p2.ui.game.PilihActivity
import kotlinx.android.synthetic.main.activity_exit.*

class ExitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exit)

        tv_mulai.setOnClickListener{
            val intent = Intent(this@ExitActivity, PilihActivity::class.java)
            startActivity(intent)
            Log.d("Exit", "Mulai")
        }
        tv_keluar.setOnClickListener{
            finishAffinity()
            Log.d("Exit", "Exit")
        }
    }
}
