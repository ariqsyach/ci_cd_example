package com.example.chap5p2.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.chap5p2.R
import com.example.chap5p2.model.register.PersonBody
import com.example.chap5p2.model.register.RegisterResponse
import com.example.chap5p2.network.ApiResource
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn_signup.setOnClickListener {
            val personBody = PersonBody(
                "${et_email.text.toString()}",
                "${et_password.text.toString()}", "${et_nama.text.toString()}"
            )
            createPerson(personBody)
//            val sp = getSharedPreferences("coba", Context.MODE_PRIVATE)
//            val esp = sp.edit()
//
//            esp.putString("name", et_nama.text.toString())
//            esp.putString("email", et_email.text.toString())
//            esp.putString("password", et_password.text.toString())
//            esp.putBoolean("isRegister", true)
//            esp.commit()

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()

        }
    }

    fun createPerson(personBody: PersonBody) {
        ApiResource.create().createPerson(personBody).enqueue(object : Callback<RegisterResponse> {
            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(
                call: Call<RegisterResponse>,
                response: Response<RegisterResponse>
            ) {
                if (response.code() == 201) {
                    Toast.makeText(this@RegisterActivity, response.body()?.message?.toString(), Toast.LENGTH_SHORT).show()
                } else if (response.code() == 422){
                    Toast.makeText(this@RegisterActivity, response.body()?.message?.toString(), Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(this@RegisterActivity, "Register Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

  //  val sp = getSharedPreferences("coba", Context.MODE_PRIVATE)

//        if ( sp.getBoolean("isLogin", false)){
//            val intent = Intent(this, LoginActivity::class.java)
//            startActivity(intent)
//            finish()
//        } else if(sp.getBoolean("isRegister", false)){
//            val intent = Intent(this, LoginActivity::class.java)
//            startActivity(intent)
//            finish()
//        }


}

