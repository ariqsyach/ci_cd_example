package com.example.chap5p2.ui.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import com.example.chap5p2.ui.navigation.BottomNavigationActivity
import com.example.chap5p2.R
import com.example.chap5p2.model.login.LoginResponse
import com.example.chap5p2.model.login.UserLogin
import com.example.chap5p2.network.ApiResource
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        var clicked = true

        btn_login.setOnClickListener {
            val userLogin =
                UserLogin(
                    "${et_emailLogin.text.toString()}",
                    "${et_passwordLogin.text.toString()}"
                )
            loginPerson(userLogin)
        }
//            if (sp.getString("name", "") == et_usernameLogin.text.toString()
//                && sp.getString("password", "") == et_passwordLogin.text.toString()
//            ) {
//
//                esp.putBoolean("isLogin", true)
//                esp.commit()
//
//                val intent = Intent(this, PilihActivity::class.java)
//                startActivity(intent)
//                finish()
//            } else {
//                Toast.makeText(this, "Username dan Password salah !", Toast.LENGTH_SHORT).show()
//            }

        btn_reset.setOnClickListener {
            et_emailLogin.text.clear()
            et_passwordLogin.text.clear()
        }
        btn_hide.setOnClickListener {

            if (clicked == true) {
                et_passwordLogin.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                clicked = false
            } else {
                et_passwordLogin.setTransformationMethod(PasswordTransformationMethod.getInstance())
                clicked = true
            }
        }
        tv_toRegist.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun loginPerson(userLogin: UserLogin) {
        ApiResource.create().loginPerson(userLogin).enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                val userId = response.body()?.data?.id.toString()
                val userName = response.body()?.data?.username.toString()
                val sp = getSharedPreferences("coba", Context.MODE_PRIVATE)
                val esp = sp.edit()
                esp.putString("user_id",userId)
                esp.putString("user_name",userName)
                esp.commit()
                if (response.code() == 200) {
                    val intent = Intent(this@LoginActivity, BottomNavigationActivity::class.java)
                startActivity(intent)
                finish()
                    Toast.makeText(
                        this@LoginActivity,
                        response.body()?.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        this@LoginActivity,
                        response.body()?.message.toString(),
                        Toast.LENGTH_SHORT
                    )
                }
            }
        })
    }

}