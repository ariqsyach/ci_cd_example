package com.example.chap5p2.ui.start

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.chap5p2.presenter.AdapterViewPager
import com.example.chap5p2.R
import kotlinx.android.synthetic.main.activity_landing_page.*

class LandingPageActivity : AppCompatActivity() {
    companion object {
        var TAG = ""
    }
    var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "On Create")
        setContentView(R.layout.activity_landing_page)


        val adapterView =
            AdapterViewPager(supportFragmentManager)
        view_pager.adapter = adapterView
        dots_indicator.setViewPager(view_pager)
  }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "On Start")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "On Resume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "On Pause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "On Stop")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "On Destroy")
    }
}




