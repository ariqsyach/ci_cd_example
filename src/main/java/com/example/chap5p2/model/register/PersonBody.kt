package com.example.chap5p2.model.register


import com.google.gson.annotations.SerializedName

data class PersonBody(
    @SerializedName("email")
    var email: String?=null,
    @SerializedName("password")
    var password: String?=null,
    @SerializedName("username")
    var username: String?=null
)