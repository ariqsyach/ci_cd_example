package com.example.chap5p2.model.login


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("email")
    var email: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("username")
    var username: String?
)