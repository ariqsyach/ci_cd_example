package com.example.chap5p2.model.login


import com.google.gson.annotations.SerializedName

data class UserLogin(
    @SerializedName("email")
    var email: String?,
    @SerializedName("password")
    var password: String?
)