package com.example.chap5p2.model.update


import com.google.gson.annotations.SerializedName

data class UserUpdate(
    @SerializedName("email")
    var email: String?=null,
    @SerializedName("username")
    var username: String?=null
)