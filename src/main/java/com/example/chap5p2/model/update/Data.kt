package com.example.chap5p2.model.update


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("CreatedAt")
    var createdAt: String?,
    @SerializedName("DeletedAt")
    var deletedAt: Any?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("hash_password")
    var hashPassword: String?,
    @SerializedName("ID")
    var iD: Int?,
    @SerializedName("password")
    var password: String?,
    @SerializedName("UpdatedAt")
    var updatedAt: String?,
    @SerializedName("username")
    var username: String?
)