package com.example.chap5p2.model.update


import com.google.gson.annotations.SerializedName

data class UpdateResponse(
    @SerializedName("data")
    var `data`: Data?,
    @SerializedName("result")
    var result: String?
)