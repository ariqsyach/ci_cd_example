package com.example.chap5p2.network

import com.example.chap5p2.model.login.LoginResponse
import com.example.chap5p2.model.register.PersonBody
import com.example.chap5p2.model.register.RegisterResponse
import com.example.chap5p2.model.login.UserLogin
import com.example.chap5p2.model.update.UpdateResponse
import com.example.chap5p2.model.update.UserUpdate
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @POST("register")
    fun createPerson(@Body personBody: PersonBody) : Call<RegisterResponse>

    @POST("login")
    fun loginPerson(@Body userLogin: UserLogin) : Call<LoginResponse>

    @GET("{id}")
    fun getPersonById(@Path("id")id:String):Call<LoginResponse>

    @PUT("{id}")
    fun updatePerson(@Body userUpdate: UserUpdate,@Path("id")id: String):Call<UpdateResponse>
}