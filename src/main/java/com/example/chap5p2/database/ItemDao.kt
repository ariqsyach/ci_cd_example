package com.example.chap5p2.database

import androidx.room.*

@Dao
interface ItemDao {
    @Query("SELECT * FROM items_table ORDER BY id ASC")
    suspend fun getItems() : List<Items>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(items : Items)

    @Update
    suspend fun update(items: Items)

    @Delete
    suspend fun delete(items: Items)

    @Query("DELETE FROM items_table")
    suspend fun deleteAll() : Int
}