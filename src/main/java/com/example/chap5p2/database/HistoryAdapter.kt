package com.example.chap5p2.database

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chap5p2.R
import kotlinx.android.synthetic.main.item_layout.view.*

class HistoryAdapter(private val listItem: List<Items>) :
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

//    private lateinit var listener: OnClickListenerCallback
//    fun setOnClickListener(onClickListenerCallback: OnClickListenerCallback){
//        this.listener = onClickListenerCallback
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listItem[position], position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(items: Items, position: Int) {
            itemView.tv_tanggal.text = items.title
            itemView.tv_hasil.text = items.desk

        }
    }

}