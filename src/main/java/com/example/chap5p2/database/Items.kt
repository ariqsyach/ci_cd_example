package com.example.chap5p2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "items_table")
data class Items(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Int? = 0,

    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "deskripsi")
    var desk: String
)
