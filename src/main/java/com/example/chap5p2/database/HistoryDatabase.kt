package com.example.chap5p2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.InternalCoroutinesApi

@Database(entities = [Items::class] , version = 1, exportSchema = false)
abstract class HistoryDatabase : RoomDatabase(){
    abstract fun itemDao() : ItemDao

    companion object{
        //agar tidak tersimpan di cache
        @Volatile
        private var INSTANCE : HistoryDatabase? = null
        @InternalCoroutinesApi
        fun getDatabase(context: Context): HistoryDatabase{
            val tempInstance = INSTANCE
            if (tempInstance!=null){
                return tempInstance
            }
            kotlinx.coroutines.internal.synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HistoryDatabase::class.java,
                    "my_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}